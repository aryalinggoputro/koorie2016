<?php namespace artsvicSupplemental;


class Buttons {

    /**
     * contact_link_button
     *
     * @param $atts
     * @return string
     */

    public static function contact_link_button( $atts )
    {
        $current_post_id = get_the_ID();
        $package = $current_post_id .',';

        $package .= ( isset( $atts['reference'] ) && ( $atts['reference'] != '' ) ) ?
            $atts['reference'] : '';

        $url = get_site_url() .'/'. get_option( 'av_supp_contact_path', '' );

        $css = ( isset($atts['css']) && ($atts['css'] != '') ) ?
            ' class="'.$atts['css'] .'"' : '';

        $encoded_package = base64_encode( $package );
        $html = sprintf( '<a href="%s?cntref=%s"%s>%s</a>',
            $url, $encoded_package, $css,
            __( (isset($atts['label']) && ! empty($atts['label']) ) ?
                $atts['label'] : 'Enquire', TEXT_DOMAIN) );

        return $html;
    }
}