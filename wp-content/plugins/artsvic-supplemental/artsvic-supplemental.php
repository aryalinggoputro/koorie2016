<?php namespace artsvicSupplemental;

    /**
     * Plugin Name:       Arts Victoria Supplemental
     * Plugin URI:        http://fatfish.com.au
     * Description:       This plugin contains functions, views and data models used for custom themes developed by fatfish.
     * Version:           0.0.1
     * Author:            Daniel Stiles
     * Author URI:        http://fatfish.com.au
     * Text Domain:       fatfish_supplemental
     * Domain Path:       /languages
     */

// Deny direct file access
if ( ! defined('WPINC') ) die;

require_once '_dev-utilities.php';

// Require base plugin class
require_once plugin_dir_path( __FILE__ ).'core/base.php';

// Load plugin's foundation
global $artsvicSupplemental;
$artsvicSupplemental = new Base();

/**
 * This performs a basic migration and populates plugin options on
 * activation.
 */
register_activation_hook( __FILE__, function() use( $artsvicSupplemental ) {

    $artsvicSupplemental->activate();

} );