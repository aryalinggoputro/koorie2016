<?php namespace artsvicSupplemental;

class Debugger {

    /**
     * @param $filePath
     * @param $content
     * @return void
     */
    private static  function writeToFile($filePath, $content)
    {
        $fd = fopen($filePath, 'a');
        // write string
        fwrite($fd, $content);
        // close file
        fclose($fd);
    }

    // Debugging support

    /**
     * @param $name
     * @param $message
     * @return void
     */
    public static function addToLog($name, $message)
    {
        $stringToWrite = '';
        $message = "[" . date("Y-m-d h:i:s", mktime()) . " ". $name ."] \n\n" . $message . "\n\n[EoM]\n\n\n";
        $filePath = ABSPATH .'logs/'.date('Y-m-d', time()).'_'.$name.'.log.php';

        if(!file_exists($filePath))
        {
            $stringToWrite = "<?php\n\ndie('Silence is golden.'); ?>\n\n";
        }

        $stringToWrite .= $message;

        self::writeToFile($filePath, $stringToWrite);
    }

    public static function dumpToLog($name, $object)
    {
        self::addToLog($name, print_r($object, 1) );
    }

}