<?php namespace artsvicSupplemental;


class GForms_Contact extends GFormsConnector {

    protected function add_filters()
    {
        add_filter( 'gform_pre_render', array( $this, 'modify_fields' ), 15, 1 );
    }

    public function modify_fields( $form )
    {
        $reference_override = null;
        $referring_post_title = null;

        // Decode and parse the reference if is present
        if( isset( $_GET['cntref'] ) && ( $_GET['cntref'] ) )
        {
            $contact_reference_raw = base64_decode( $_GET['cntref'] );
            unset( $_GET['cntref'] );

            // If a valid reference format, parse into an array
            if( substr_count( $contact_reference_raw, ',' ) == 1 )
            {
                $contact_reference_arr = explode( ',', $contact_reference_raw );

                // Check for a reference override
                if( $contact_reference_arr[1] != '' )
                {
                    $reference_override = $contact_reference_arr[1];

                // No override, so get the referring post ID and perform lookup
                } else {

                    // Check that it is an integer
                    if( is_numeric( $contact_reference_arr[0] ) )
                    {
                        $referring_post = get_post( $contact_reference_arr[0] );

                        if( is_object($referring_post) && is_a( $referring_post, 'WP_Post' ) )
                        {
                            $referring_post_title = $referring_post->post_title;
                        }
                    }
                }
            }

            unset( $contact_reference_raw );
        }

        // Iterate through the form fields and retaining reference
        foreach( $form['fields'] as &$field )
        {
            // Find the subject select field and set the option or just the selected attribute
            if( $this->check_field_identifier( 'subject', $field['cssClass'] ) )
            {
                if( ! is_null( $reference_override ) )
                {
                    $field['choices'][] = array(
                        'text' => $reference_override,
                        'value' => $reference_override,
                        'isSelected' => 1
                    );

                } else {

                    $general_key = 0;
                    $overridden = false;

                    // Iterate through the options
                    for( $i = 0; $i < count( $field['choices'] ); $i++ )
                    {
                        // Default the is selected attribute to null
                        $field['choices'][$i]['isSelected'] = null;
                        $general_key = ( $field['choices'][$i]['text'] == 'General' ) ? $i : 0;

                        // Check if the referring post title matches the option and select if it does
                        if( ! is_null( $referring_post_title ) )
                        {
                            if( strtolower( $field['choices'][$i]['text'] ) == strtolower( $referring_post_title ) )
                            {
                                $field['choices'][$i]['isSelected'] = 1;
                                $overridden = true;
                            }
                        }
                    }

                    // Should the selection not be made, set the 'General' option to the selected option
                    if( ! $overridden )
                    {
                        $field['choices'][$general_key]['isSelected'] = 1;
                    }
                }
            }

            // Prevent overlap
            unset( $field );
        }

        return $form;
    }
}