<?php namespace artsvicSupplemental;

// Load connectors
require_once 'contact.php';

$gFormsConnector = new GFormsConnector();

class GFormsConnector {

    protected
        $form_id,
        $identifier,
        $css_class_prefix = 'artsvicGform-',
        $form_prefix = 'form-',
        $field_prefix = 'field-';

    public function __construct( $form_id = 0, $identifier = '' )
    {
        // Used for when instantiated within a child object
        if($form_id > 0)
        {
            $this->form_id = $form_id;
            $this->identifier = $identifier;

            // Should be the child object's override method
            $this->add_filters();

        // Activate the hooks to detect the form
        } else {

            $this->add_filters();
        }
    }

    protected function add_filters()
    {
        add_action( 'gform_pre_process', array( $this, 'detect_form' ), 1, 1);
        add_filter( 'gform_pre_render', array( $this, 'detect_form' ), 1, 1 );
    }

    /**
     * detect_form
     *
     * @param $form
     * @return mixed
     */
    public function detect_form( $form )
    {
        // Extract the form identifier string
        $form_identifier = str_replace( $this->css_class_prefix . $this->form_prefix, '', $form['cssClass'] );
        $form_connector = null;

        // Instantiate the detected form's connector class
        switch($form_identifier)
        {
            case 'contact':

                $form_connector = new GForms_Contact( $form['id'], $form_identifier );

                break;

            default:
                break;
        }

        return $form;
    }

    /**
     * check_field_identifier
     *
     * @param $identifier
     * @param $css_class
     * @return bool
     */
    protected function check_field_identifier($identifier, $css_class)
    {
        if( substr_count( $css_class, $identifier ) > 0 )
        {
            return true;
        }

        return false;
    }
}