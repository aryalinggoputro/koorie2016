<?php namespace artsvicSupplemental;

/**
 * Class Base
 *
 * The purpose of this class is to setup the foundation of all functions,
 * define helpers, manage dependencies and instantiate the loader.
 *
 * @package artsvicSupplemental
 */

class Base {

    protected $loader;
    protected $slug;
    protected $version;

    public function __construct()
    {
        // Set the plugin slug and version
        // Attributes are for internal class use, constants are for global use
        $this->slug = 'ARTSVIC_SUPPLEMENTAL';
        $this->version = '0.0.1';

        // Initialise options

        // Define and store plugin version for upgrade use
        if ( ! defined('ARTSVIC_SUPPLEMENTAL__VERSION_SLUG') )
            define('ARTSVIC_SUPPLEMENTAL__VERSION_SLUG', $this->slug);

        if ( ! defined('ARTSVIC_SUPPLEMENTAL__VERSION_NUM') )
            define('ARTSVIC_SUPPLEMENTAL__VERSION_NUM', $this->version);

        // Setup foundation
        $this->config();

        // Load dependencies (loader and third party)
        $this->load_dependencies();
    }

    private function config()
    {
        // Paths
        define('ARTSVIC_SUPPLEMENTAL__PLUGIN_BASE_DIR',     dirname(plugin_dir_path(__FILE__) ) .'/' );
        define('ARTSVIC_SUPPLEMENTAL__VENDOR_DIR',          ARTSVIC_SUPPLEMENTAL__PLUGIN_BASE_DIR.'vendor/');
        define('ARTSVIC_SUPPLEMENTAL__VIEW_DIR',            ARTSVIC_SUPPLEMENTAL__PLUGIN_BASE_DIR.'views/');
        define('ARTSVIC_SUPPLEMENTAL__LOGIC_DIR',           ARTSVIC_SUPPLEMENTAL__PLUGIN_BASE_DIR.'core/');
        define('ARTSVIC_SUPPLEMENTAL__MODELS_DIR',          ARTSVIC_SUPPLEMENTAL__PLUGIN_BASE_DIR.'database/models/');
        define('ARTSVIC_SUPPLEMENTAL__MIGRATIONS_DIR',      ARTSVIC_SUPPLEMENTAL__PLUGIN_BASE_DIR.'database/migrations/');
        define('ARTSVIC_SUPPLEMENTAL__ELEMENTS_DIR',        ARTSVIC_SUPPLEMENTAL__PLUGIN_BASE_DIR.'elements/');
        define('ARTSVIC_SUPPLEMENTAL__GFORMS_DIR',          ARTSVIC_SUPPLEMENTAL__PLUGIN_BASE_DIR.'gforms/');

        // IDs
        define('ARTSVIC_SUPPLEMENTAL__BLOG_ID', get_current_blog_id());

        // Load the text domain
        load_plugin_textdomain('fatfish_supplemental', false, ARTSVIC_SUPPLEMENTAL__PLUGIN_BASE_DIR.'languages');
    }

    private function load_dependencies()
    {
        // Load elements static classes
        require_once ARTSVIC_SUPPLEMENTAL__ELEMENTS_DIR.'elements.php';

        // Load shortcodes
        require_once ARTSVIC_SUPPLEMENTAL__LOGIC_DIR.'shortcodes.php';

        // Load Gravity Forms connector
        require_once ARTSVIC_SUPPLEMENTAL__GFORMS_DIR.'connector.php';

        // Load the loader
        require_once ARTSVIC_SUPPLEMENTAL__LOGIC_DIR.'loader.php';
        $this->loader = new Loader();
    }

    public function activate()
    {
        // $this->run_install();
    }

    public function run_install()
    {
        $this->loader->run_database_migration();
    }
}