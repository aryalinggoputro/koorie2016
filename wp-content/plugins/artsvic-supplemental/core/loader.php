<?php namespace artsvicSupplemental;

/**
 * Class Loader
 *
 * Defines all provided hook callbacks at runtime and
 * handles database migrations
 *
 * @package mmiMSAuctions
 */

class Loader
{
    public static $migration_sql;

    public function __constructor()
    {
        //
    }

    /**
     * run_database_migration
     *
     * Should only be ran during the install plugin phase
     *
     * @void
     */
    public function run_database_migration()
    {
        $this->migrate_database();
    }

    /**
     * migrate_database
     *
     * Migration function that will include all migration files (one for each table)
     * and attempt to create the relative tables
     *
     * Notes from codex on creating tables in WP:
     * - You must put each field on its own line in your SQL statement.
     * - You must have two spaces between the words PRIMARY KEY and the definition of your primary key.
     * - You must use the key word KEY rather than its synonym INDEX and you must include at least one KEY.
     * - You must not use any apostrophes or backticks around field names.
     * - Field types must be all lowercase.
     *
     * @void
     */

    private static function migrate_database()
    {
        global $wpdb;
        require_once(ABSPATH.'wp-admin/includes/upgrade.php');

        // Set default charset and collation
        $charset_collate = '';

        if ( ! empty( $wpdb->charset ) )
        {
            $charset_collate .= 'DEFAULT CHARACTER SET '.$wpdb->charset;
        }

        if (!empty( $wpdb->collate ) )
        {
            $charset_collate .= ' COLLATE '.$wpdb->collate;
        }

        foreach ( glob( ARTSVIC_SUPPLEMENTAL__MIGRATIONS_DIR.'*.php' ) as $migrationFile)
        {
            if ( is_readable( $migrationFile ) )
            {
                self::$migration_sql = null;

                include_once $migrationFile;

                self::$migration_sql .= ' ENGINE = InnoDB '.$charset_collate.';';
                dbDelta( self::$migration_sql );
            }
        }
    }

    /**
     * drop_database
     *
     * Drops all migration tables
     * Loops over all migration files and runs sql DROP TABLE $table_name
     *
     * @void
     */

    private static function drop_database()
    {
        foreach ( glob( ARTSVIC_SUPPLEMENTAL__MIGRATIONS_DIR.'*.php' ) as $migrationFile )
        {
            if ( is_readable( $migrationFile ) )
            {
                include_once $migrationFile;
                global $table_name;
                dbDelta( 'DROP TABLE '.$table_name );
            }
        }
    }
}