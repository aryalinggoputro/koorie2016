<?php namespace artsvicSupplemental;

class Shortcodes {

    public function __construct()
    {
        // Bind the callbacks
        add_shortcode( 'contact_button', array( $this, 'create_contact_button' ) );
    }

    /**
     * create_contact_button
     *
     * Creates a button used to send reference data to the contact form.
     *
     * @param $atts
     * @return string
     */

    public function create_contact_button( $atts )
    {
        return Buttons::contact_link_button( $atts );
    }
}

// Instantiate and initialise the shortcodes
global $artsvicSupplemental_shortcodes;
$artsvicSupplemental_shortcodes = new Shortcodes();