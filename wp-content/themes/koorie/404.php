<?php
/**
 * @package koorie
 */

get_header(); ?>

    <!--
    ####################################
    START of MAIN CONTENT
    ####################################
    -->
    <div id="main" role="main">
        <div class="container">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding page-title">
                            <h1><?php _e('404 Page not found', TEXT_DOMAIN); ?></h1>
                            <a href="#" class="yellow-btn hidden-md hidden-lg">Donate</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <article>

                                <div class="media">
                                    <div class="media-body">
                                        <p><?php _e('The requested page could not be found.', TEXT_DOMAIN) ?></p>
                                    </div>
                                </div>
                            </article>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--
    ####################################
    END of site MAIN CONTENT
    ####################################
    -->

<?php get_footer(); ?>