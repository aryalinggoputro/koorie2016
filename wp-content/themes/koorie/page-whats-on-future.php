<?php
/**
 * Template Name: What's On - Future
 * Description: Displays future events.
 *
 * @package koorie
 */

$date_filter = 'comingSoon';

include_once('page-whats-on.php');