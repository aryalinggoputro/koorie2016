<?php
/**
 * @package koorie
 */

get_header();

// Toggle the subnav sidebar
ThemeTemplateUtilities::set_sidebar_usage( 'subnav' );

// Get the page title to be used throughout the mark up
$page_title = get_the_title();

?>

    <div id="main" role="main">

        <?php ThemeTemplateUtilities::get_partial( 'subpage-heading-mobile', array('page_title' => $page_title) ); ?>

        <div class="container">

        <?php while ( have_posts() ) : the_post(); ?>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 ">

                    <?php ThemeTemplateUtilities::get_partial( 'subpage-heading-desktop', array('page_title' => $page_title) ); ?>

                    <div class="row">
                        <?php get_sidebar(); ?>

                        <section class="col-xs-12 col-md-9">

                            <?php

                                if ( has_post_thumbnail() )
                                {
                                    // retrieve alt image : Arya on 10 May 16
                                    $alt_img = get_post_meta(get_post_thumbnail_id( $post->ID ), '_wp_attachment_image_alt', true); 
                                    $feature_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ) , "banner-size" );
                                    
                                    echo sprintf('<img src="%s" width="%s" height="%s" alt="%s" style="margin-bottom: 20px;">', $feature_image[0], $feature_image[1], $feature_image[2], $alt_img);
                                }

                            ?>

                            <br>

                            <?php the_content(); ?>

                        </section>
                    </div>

                </div>
            </div>

        <?php endwhile; ?>
        </div>


        <?php ThemeTemplateUtilities::get_partial( 'nested-scrollup' ); ?>

    </div>




<?php get_footer(); ?>
