<?php
/**
 * @package koorie
 */

$the_query = $args['the_query'];

if( $the_query->have_posts() ) :

?>

    <ul class="dark arrow-list">

        <?php

        while ( $the_query->have_posts() ) :

            $the_query->the_post();
            global $post;

            ?>
            <li>
                <a href="#<?php echo $post->post_name; ?>"><?php echo strtoupper($post->post_title); ?></a>
            </li>
            <?php

        endwhile;

        ?>

    </ul>

<?php

endif;