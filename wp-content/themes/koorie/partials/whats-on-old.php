<?php
/**
 * This is the old implementation of the sidebar for What's On.
 *
 * @package koorie
 */

// Pull the current post object off of the global scope.
global $post;

// Filter by
$date_filters = array(
    array('slug' => 'comingSoon', 'label' => 'Coming Soon', 'selected' => false),
    array('slug' => 'nowShowing', 'label' => 'Now Showing', 'selected' => false),
    array('slug' => 'pastExhibitions', 'label' => 'Past Exhibitions', 'selected' => false)
);

if( isset($_GET['filtertype']) && ! empty($_GET['filtertype']) )
{
    foreach($date_filters as &$date_filter)
    {
        if($_GET['filtertype'] == $date_filter['slug'])
        {
            $date_filter['selected'] = true;
        }

        unset($date_filter);
    }

} else {

    $date_filters[1]['selected'] = true;
}

// Year or month
$filter_by = array();
$filter_by_selected = '';

if( isset($_GET['filtertype']) && ! empty($_GET['filtertype']) )
{
    $filter_type = $_GET['filtertype'];

    if($filter_type == 'comingSoon')
    {
        $filter_by = WhatsOn::get_future_months();
    }

    if($filter_type == 'pastExhibitions')
    {
        $filter_by = WhatsOn::get_past_years();
    }

    if( isset($_GET['filterby']) && ! empty($_GET['filterby']) )
    {
        $filter_by_selected = $_GET['filterby'];
    }
}

?>
            <div class="col-xs-12 col-sm-12 col-md-3 nopadding">
                <aside>
                    <form name="event_search_options" action="" method="get" enctype="application/x-www-form-urlencoded" class="search-form">

                        <?php foreach($date_filters as $date_filter) : ?>
                            <div class="dark radio-container">

                                <input type="radio" value="<?php echo $date_filter['slug']; ?>" id="<?php echo $date_filter['slug']; ?>"
                                       name="filtertype"<?php echo ($date_filter['selected']) ? ' checked="checked"' : ''; ?>>
                                <label for="<?php echo $date_filter['slug']; ?>"><span><?php echo $date_filter['label']; ?></span></label>

                            </div>
                        <?php endforeach; ?>

                        <?php

                        if( count($filter_by) > 0 ) :

                            ?>
                            <select name="filterby">
                                <?php foreach( $filter_by as $option ) : ?>
                                    <option value="<?php echo $option['value']; ?>"<?php echo ($option['value'] == $filter_by_selected) ? ' checked="checked"' : ''; ?>><?php echo $option['label']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        <?php

                        endif;

                        ?>

                        <button class="btn-red btn-search">Search</button>
                    </form>

                </aside>
            </div>