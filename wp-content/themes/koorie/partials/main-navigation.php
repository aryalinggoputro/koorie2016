<?php
/**
 * @package koorie
 */

global $post;

$menu_items = $args['menu_items'];
unset($args);

if( ! empty($menu_items) ) :

?>

                        <nav class="main-nav">
                            <ul class="level-1">
                                <?php

                                foreach($menu_items as $menu_item) :

                                    if( ! isset($menu_item->children) || empty($menu_item->children) ) :
                                ?>
                                <li><a href="<?php echo $menu_item->url; ?>"<?php if( ($menu_item->object_id == $post->ID) || ($menu_item->title == 'Home' && $post->post_title == 'Home' ) ) echo ' class="active"'; ?>><?php echo $menu_item->title; ?></a></li>
                                <?php else : ?>
                                <li class="expand">
                                    <span><?php echo $menu_item->title; ?></span>
                                    <a href="<?php echo $menu_item->url; ?>"<?php if($menu_item->object_id == $post->ID) echo ' class="active"'; ?>><?php echo $menu_item->title; ?></a>
                                    <ul class="level-2">
                                        <?php foreach($menu_item->children as $child) : ?>
                                        <li><a href="<?php echo $child->url; ?>"<?php if($child->object_id == $post->ID) echo ' class="active"'; ?>><?php echo $child->title; ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>
                                <?php endif; endforeach; ?>
                                <li>
                                    <form action="" method="get" class="search-mobile">
                                        <legend class="hidden">Search box for mobile</legend>
                                        <input type="text" aria-label="site search" name="s" placeholder="Search" value="<?php echo get_search_query(); ?>">
                                        <i class="fa fa-search"></i>
                                    </form>
                                </li>
                            </ul>
                        </nav>

<?php endif; ?>