<?php
/**
 * @package koorie
 */

global $post;

// By default use the current post details as the parent
$parent_id = $post->ID;
$parent_label = $post->post_title;
$active_post_id = $post->ID;
$active_slug = $post->post_name;

// Determine if page sits atop children pages
if ($post->post_parent > 0) {
    // Get the parent page post object
    $parent_post = get_post($post->post_parent);
    $parent_id = $parent_post->ID;
    $parent_label = $parent_post->post_title;
}

// Get children page post objects if present
$children = ThemeTemplateUtilities::get_main_menu_item_children( $parent_id );

// Draw a menu
if (count($children) > 0) :

    ?>
    <div class="col-xs-12 col-sm-12 col-md-3  side-nav-container">

        <aside>
            <nav class="side-nav">
                <ul>
                    <?php

                    foreach ($children as $child) :

                        if ($child->post_status == 'publish') :

                            ?>
                            <li>
                                <a href="<?php echo $child->url; ?>"<?php if ( $active_post_id == $child->object_id || (  ( $args['date_filter'] == 'nowShowing' && $active_slug == 'whats-on' && $child->object_id == get_option( 'options_current_events_page_id' ) ) ) ) echo ' class="active"'; ?>>
                                    <?php echo $child->title; ?>
                                </a>
                            </li>
                        <?php

                        endif;

                    endforeach;

                    ?>
                </ul>
            </nav>

        </aside>
    </div>
<?php
endif;
