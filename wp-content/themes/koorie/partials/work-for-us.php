<article class="job-opening">

    <h2><?php the_title(); ?></h2>

    <table>
        <tbody>

        <?php

        $salary = get_field('salary', $post->ID);

        if( ! empty($salary) ) :

        ?>
        <tr>
            <th>SALARY:</th>
            <td><?php echo $salary; ?></td>
        </tr>
        <?php

        endif;

        $superannuation = get_field('superannuation', $post->ID);

        if( ! empty($superannuation) ) :

        ?>
        <tr>
            <th>SUPERANNUATION:</th>
            <td><?php echo $superannuation; ?></td>
        </tr>
        <?php

        endif;

        $employment_type = get_field('employment_type', $post->ID);

        if( ! empty($employment_type) ) :

        ?>
        <tr>
            <th>EMPLOYMENT TYPE:</th>
            <td><?php echo $employment_type; ?></td>
        </tr>
        <?php

        endif;

        $how_to_apply = get_field('how_to_apply', $post->ID);

        if( ! empty($how_to_apply) ) :

        ?>
        <tr>
            <th>HOW TO APPLY:</th>
            <td><?php echo $how_to_apply; ?></td>
        </tr>
        <?php

        endif;

        $contact_name = get_field('contact_name', $post->ID);
        $contact_phone = get_field('contact_phone', $post->ID);
        $contact_email = get_field('contact_email', $post->ID);

        if( ($contact_phone != '') && ($contact_email != '') )
        {
            $contact_phone = sprintf('(T %s; E %s)', $contact_phone, $contact_email);

        } elseif( ($contact_phone != '') && ($contact_email == '') ) {
            $contact_phone = sprintf('(T %s)', $contact_phone);

        } elseif( ($contact_phone == '') && ($contact_email != '') ) {

            $contact_phone = sprintf('(E %s)', $contact_email);
        }

        if( ! empty($contact_name) || ! empty($contact_phone) || ! empty($contact_email) ) :

        ?>
        <tr>
            <th>CONTACT FOR ENQUIRIES ONLY:</th>
            <td><?php echo $contact_name; ?> <?php echo $contact_phone; ?></td>
        </tr>
        <?php

        endif;

        $closing_date = get_field('closing_date', $post->ID);

        if( ! empty($closing_date) ) :

        ?>
        <tr>
            <th>CLOSING DATE FOR APPLICATIONS:</th>
            <td><?php echo $closing_date; ?></td>
        </tr>
        <?php
        endif;
        ?>

        </tbody>
    </table>


    <?php the_content(); ?>

    <p><?php echo get_field('mailing_instruction', $post->ID); ?></p>

    <p><?php echo get_field('contact_mailing_address', $post->ID); ?></p>

    <?php echo \artsvicSupplemental\Buttons::contact_link_button(array('label' => 'Contact Us', 'css' => 'btn-large', 'reference' => 'Position for '.get_the_title() ) ); ?>
    <!--<a href="contact-us.html" class="btn-large">Contact us</a>-->

    <?php
    if( get_field('document_download', $post->ID) ) :

        $attachment_doc = get_field('document_download', $post->ID);

        if($attachment_doc) :
            $url = $attachment_doc['url'];
            $title = $attachment_doc['title'];
            $mime = $attachment_doc['mime_type'];
            $file_path = str_replace(get_site_url(), rtrim(ABSPATH, '/'), $url);
            ?>
            <p>Download a copy of the position description by clicking on the link below:</p>
            <a href="<?php echo $url; ?>" class="dark icon-link"><i class="fa fa-file-pdf-o"></i><?php echo $title; echo ($mime == 'application/pdf') ? ' (PDF) ' : ''; echo ' '. strtoupper(ThemeTemplateUtilities::filesize_formatted($file_path) ); ?></a>
        <?php endif; endif; ?>



    <!--<a href="#" class="dark icon-link"><i class="fa fa-file-pdf-o"></i>PD Senior Collections Manager (PDF) 360.88kB</a>-->
</article>
