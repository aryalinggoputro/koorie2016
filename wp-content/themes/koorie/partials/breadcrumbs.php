<?php
/**
 * @package koorie
 */

$breadcrumbs = $args['breadcrumbs'];
unset($args);

if( is_a($breadcrumbs, 'Breadcrumbs' ) && $breadcrumbs->has_crumbs() ) :

?>

                <div class="breadcrumbs-container">

                    <ol class="breadcrumbs">
                        <?php foreach($breadcrumbs->crumbs as $crumb) : ?>
                        <li><a href="<?php echo $crumb['url']; ?>"><?php echo $crumb['label']; ?></a></li>
                        <?php endforeach; ?>
                    </ol>

                    <?php if( ! empty( $breadcrumbs->back_url ) ) : ?>
                    <a href="<?php echo $breadcrumbs->back_url; ?>" class="back-btn">
                        <i class="fa fa-angle-left"></i>
                        <span><?php _e('Back to List'); ?></span>
                    </a>
                    <?php endif; ?>

                </div>

<?php endif ?>