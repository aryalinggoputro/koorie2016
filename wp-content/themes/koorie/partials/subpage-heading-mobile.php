<?php
/**
 * @package koorie
 */
?>

    <div class="container-fluid mobile-page-tite hidden-md hidden-lg ">

        <div class="col-xs-12 col-sm-12 no-padding">
            <div class="page-title">
                <h1>
                  <?php echo get_the_title();
                  //echo $args['page_title'];

                  ?>
                </h1>
                <a href="<?php echo get_option('options_donate_url'); ?>" class="yellow-btn">Donate</a>
            </div>
        </div>

    </div>
