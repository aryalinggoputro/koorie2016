<?php
/**
 * @package koorie
 */
?>

<div class="container">
    <div class="row">
        <div class="col-md-1 col-md-offset-10">
            <a class="pull-right up-arrow" href="#top"><i class="fa fa-angle-up"></i></a>
        </div>
    </div>
</div>