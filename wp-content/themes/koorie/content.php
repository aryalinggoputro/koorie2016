<?php
/**
 * @package koorie
 */

global $post;

?>

                <article class="content-feature-event-detail">

                    <?php echo get_the_post_thumbnail( $post->ID, 'hero-event-listing' ); ?>

                    <?php the_content(); ?>

                </article>