<?php
/**
 * @package koorie
 */

get_header();

// Toggle the subnav sidebar
ThemeTemplateUtilities::set_sidebar_usage( 'subnav' );

// Get the post object
global $post;

$page_title = ($post->post_type == 'event') ? 'What\'s On' : $post->post_title;

?>

    <div id="main" role="main">

        <?php ThemeTemplateUtilities::get_partial( 'subpage-heading-mobile', array('page_title' => $page_title) ); ?>

        <div class="container">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 ">

                    <?php ThemeTemplateUtilities::get_partial( 'subpage-heading-desktop', array('page_title' => $page_title) ); ?>

                    <?php

                    if($post->post_type == 'event')
                    {
                        get_template_part('content', 'event-detailed');

                    }else if($post->post_type == 'publications')
                    {
                        get_template_part('content', 'pub-detailed');

                    } else {

                        get_template_part('content');
                    }

                    ?>

                </div>
            </div>
        </div>


        <?php ThemeTemplateUtilities::get_partial( 'nested-scrollup' ); ?>

    </div>

<?php get_footer(); ?>
