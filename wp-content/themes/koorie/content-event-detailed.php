<?php
/**
 * @package koorie
 */

global $post;
$paged = 0;
$event_type = wp_get_post_terms( $post->ID, 'event_types' );
$event_type_label = '';

if( isset($event_type[0]) && is_object($event_type[0]) ) {
    $event_type_label = $event_type[0]->name;
}

// DATE FORMATTING --> Get and format the start and end dates for event
$start_date_raw = get_field( 'start_date', $post->ID );
$end_date_raw = get_field( 'end_date', $post->ID );

$duration_dates = WhatsOn::format_duration( $start_date_raw, $end_date_raw, 'detailed' );

if( isset($_GET['epage'] ) && ( $_GET['epage'] > 0) ) {

    $paged = (int)$_GET['epage'];
}

$current_permalink = get_permalink( get_option( 'options_current_events_page_id' ) );

$breadcrumbs = ThemeTemplateUtilities::get_breadcrumbs();
$breadcrumbs->add_crumb( $current_permalink, 'What\'s On' );

// Determine past, present or future
$start_date_time = strtotime( $start_date_raw .' 00:00:00' );
$end_date_time = strtotime( $end_date_raw . ' 23:59:59' );
$current_time = time();

$back_label = '';

if( ( $current_time >= $start_date_time ) && ( $end_date_time > $current_time ) ) {

    $back_label = 'Current '.$event_type_label;
    $back_url = $current_permalink;

} elseif( $current_time > $end_date_time ) {

    $back_label = 'Previous '.$event_type_label;
    $back_url = get_permalink( get_option( 'options_past_events_page_id' ) );

} else {

    $back_label = 'Future '.$event_type_label;
    $back_url = get_permalink( get_option( 'options_future_events_page_id' ) );
}

$back_url .= ($paged > 0) ? 'page/'. $paged .'/' : '';

$breadcrumbs->add_crumb( $back_url, $back_label );
$breadcrumbs->add_crumb( '', get_the_title() );
$breadcrumbs->back_url = $back_url;
$breadcrumbs->show();

?>

                <article class="content-feature-event-detail">

                    <?php echo $duration_dates; ?>

                    <h5><?php echo strtoupper($event_type_label); ?></h5>

                    <h2><?php echo strtoupper( get_the_title() ); ?></h2>

                    <?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>

                    <?php the_content(); ?>

                </article>