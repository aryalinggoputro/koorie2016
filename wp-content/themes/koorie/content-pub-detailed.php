<?php
/**
 * @package koorie
 */

global $post;
$paged = 0;
$event_type = wp_get_post_terms( $post->ID, 'event_types' );
$event_type_label = '';

if( isset($event_type[0]) && is_object($event_type[0]) ) {
    $event_type_label = $event_type[0]->name;
}

// DATE FORMATTING --> Get and format the start and end dates for event
$start_date_raw = get_field( 'start_date', $post->ID );
$end_date_raw = get_field( 'end_date', $post->ID );

$duration_dates = WhatsOn::format_duration( $start_date_raw, $end_date_raw, 'detailed' );

if( isset($_GET['epage'] ) && ( $_GET['epage'] > 0) ) {

    $paged = (int)$_GET['epage'];
}

$current_permalink = get_permalink( $post->ID );
$pub_permalink = get_permalink( 1001 );

$breadcrumbs = ThemeTemplateUtilities::get_breadcrumbs();
$breadcrumbs->add_crumb( $pub_permalink, 'Publications &amp; Resources' );

// Determine past, present or future
$start_date_time = strtotime( $start_date_raw .' 00:00:00' );
$end_date_time = strtotime( $end_date_raw . ' 23:59:59' );
$current_time = time();

$breadcrumbs->add_crumb( $current_permalink, get_the_title() );
$breadcrumbs->show();

?>

                <article class="content-feature-event-detail">

                    <?php echo $duration_dates; ?>

                    <h5><?php echo strtoupper($event_type_label); ?></h5>

                    <h2><?php echo strtoupper( get_the_title() ); ?></h2>

                    <?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>

                    <?php the_content(); ?>

                </article>