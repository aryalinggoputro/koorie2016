<?php
/**
 * The backbone of the whole theme.
 *
 * @package koorie
 */

if(WP_DEBUG) {

    error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    ini_set('display_errors', 1);

} else {

    error_reporting(E_ERROR);
    ini_set('display_errors', 0);
}


define('THEME_URL', get_template_directory_uri());
define('THEME_DIR', get_template_directory());
define('WPACCESS_ADMIN_ROLE', 'administrator');
define('THEME_INCLUDES_DIR', THEME_DIR .'/includes/');

// Include dependencies
require THEME_INCLUDES_DIR .'template-utilities.php';

// Initialise the sidebar usage flags
ThemeTemplateUtilities::init_sidebar_usage();

// Bring in models
require THEME_INCLUDES_DIR .'models/main-menu.php';
require THEME_INCLUDES_DIR .'models/breadcrumbs.php';
require THEME_INCLUDES_DIR .'models/nav-menu.php';

// Strap in the What's On support class
require THEME_INCLUDES_DIR . 'whats-on.php';


if( ! function_exists( 'run_theme_setup' ) ) {

    function run_theme_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on twentyfifteen, use a find and replace
         * to change 'twentyfifteen' to the name of your theme in all the template files
         */
        load_theme_textdomain( 'koorie', get_template_directory() . '/languages' );

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        // Add menus support
        add_theme_support('menus');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support( 'post-thumbnails' );
        set_post_thumbnail_size( 825, 510, true );

        // Set image sizes
        add_image_size('large', 1180, '', true); // Large Thumbnail
        add_image_size('medium', 880, '', true); // Medium Thumbnail
        add_image_size('small', 580, '', true); // Small Thumbnail
        add_image_size('thumb', 300, '', true); // Small Thumbnail
        add_image_size('home-slider-banner', 1170, 600, true);
        add_image_size('home-slider-feature', 300, 300, true);
        add_image_size('hero-img', 350, 150, true);
        add_image_size('hero-event-listing', 170, 170, true);
        add_image_size('post-feature-right-size', 400, 250, true);
        add_image_size('sponsor-size', 277, 150, true);
        add_image_size('banner-size', 848, 363, true);

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
        ) );

        /*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
        add_theme_support( 'post-formats', array(
            'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio'
        ) );

        // Add post types
        add_post_type_support('page', 'excerpt');

        /*
         * This theme styles the visual editor to resemble the theme style,
         * specifically font, colors, icons, and column width.
         */

        // add_editor_style( array( 'assets/build/css/editor-style.css' ));
        register_nav_menu( 'header-navigation', 'Header Navigation' );
        register_nav_menu( 'top-navigation', 'Top Navigation' );
        register_nav_menu( 'footer-navigation', 'Footer Navigation' );
    }
}

add_action( 'after_setup_theme', 'run_theme_setup' );

// enqueue scripts and styles

function koorie_scripts() {
//  wp_enqueue_style( 'style-name', get_stylesheet_uri() );
    wp_enqueue_script( 'korrie_js', get_template_directory_uri() . '/assets/build/js/scripts.js', array(), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'koorie_scripts' );

// Remove admin bar
function remove_admin_bar() {

    return false;
}

add_filter('show_admin_bar', 'remove_admin_bar');

// Making it more of an aussie feel
add_filter('gettext', 'change_howdy', 10, 3);

function change_howdy($translated, $text, $domain) {

    if (!is_admin() || 'default' != $domain)
        return $translated;

    if (false !== strpos($translated, 'Howdy'))
        return str_replace('Howdy', 'G\'day', $translated);

    return $translated;
}


/*
 * Modifying TinyMCE editor to remove unused items.
 */
function customformatTinyMCE($init) {
  // Add block format elements you want to show in dropdown
  $init['block_formats'] = "Paragraph=p;Header 2=h2;Header 3=h3;Header 4=h4;Header 5=h5;Header 6=h6;";

  return $init;
}

// Modify Tiny_MCE init
add_filter('tiny_mce_before_init', 'customformatTinyMCE' );

// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {
  // Define the style_formats array
  $style_formats = array(
    // Each array child is a format with it's own settings
    array(
      'title' => 'Dark PDF Box',
      'inline' => 'a',
      'classes' => 'dark icon-link pdf-icon',
      'wrapper' => true,

    ),
    array(
      'title' => 'Dark Link Box',
      'inline' => 'a',
      'classes' => 'dark icon-link link-icon',
      'wrapper' => true,
    ),
    array(
      'title' => 'Arrow List',
      'block' => 'ul',
      'classes' => 'dark arrow-list',
      'wrapper' => true,
    ),
    array(
      'title' => 'Dark Arrow List',
      'block' => 'ul',
      'classes' => 'arrow-list',
      'wrapper' => true,
    ),
  );
  // Insert the array, JSON ENCODED, into 'style_formats'
  $init_array['style_formats'] = json_encode( $style_formats );

  return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

define('CONCATENATE_SCRIPTS', false);
