<?php
/**
 * @package koorie
 */

get_header();

$page_title = 'Home';

?>

<div id="main" role="main">

    <?php ThemeTemplateUtilities::get_partial( 'subpage-heading-mobile', array('page_title' => $page_title) ); ?>

    <?php

    // Get the slides for the home page
    $slides_query_args = array(
        'post_type' => 'hompageslide',
        'post_status' => 'publish',
        'meta_key'       => 'slide_order',
        'orderby'        => 'meta_value_num',
        'order'          => 'ASC'
    );

    $slides_query = new WP_Query( $slides_query_args );

    if( $slides_query->have_posts() ) :

        $slide_count = 0;
    ?>
    <section class="hero">
        <div class="container-fluid">
            <div class="row">
                <div class="">

                    <div class="carousel-container flexslider">

                        <ul class="carousel slides">

                            <?php

                            while ( $slides_query->have_posts() ) :

                                $slides_query->the_post();

                                global $post;
                                $slide_count++;

                                $background_image = get_field( 'slide_background', $post->ID );

                                if( get_field( 'slide_feature_position' ) == 'img-circle-right' || get_field( 'slide_feature_position' ) == 'img-square-right'){
                                  $positionImgClassList = ' col-xs-12 col-md-3 col-md-push-6 ';
                                  $positionContentClassList = ' tagline col-xs-12 col-md-6 col-md-pull-3 ';
                                }

                                if( get_field( 'slide_feature_position' ) == 'img-circle-left' || get_field( 'slide_feature_position' ) == 'img-square-left'){
                                  $positionImgClassList = ' col-xs-12 col-md-3 ';
                                  $positionContentClassList = ' tagline col-xs-12 col-md-6 ';
                                }
                            ?>
                            <li data-position="<?php echo $slide_count; ?>">
                                <?php if( isset($background_image['sizes']['home-slider-banner']) ) : ?><img src="<?php echo $background_image['sizes']['home-slider-banner']; ?>"><?php endif; ?>

                                <div class="container">
                                  <div class="row">

                                   <div class="inner  col-xs-12">
                                     <div class="row">

                                       <div class="<?php echo $positionImgClassList; ?>">
                                            <?php if(get_the_post_thumbnail( $post->ID, 'home-slider-feature' )): ?>
                                                 <figure class="<?php echo get_field( 'slide_feature_position' ); ?>  ">
                                                     <?php echo get_the_post_thumbnail( $post->ID, 'home-slider-feature' ); ?>
                                                 </figure>
                                           <?php endif;?>
                                       </div>
                                        <div class="<?php echo $positionContentClassList; ?>">
                                            <span><?php $category = get_the_category(); echo $category[0]->cat_name; ?></span>
                                            <h2 class="h2-large"><?php the_title(); ?></h2>
                                            <p><?php $content = get_the_content(); echo strip_tags( $content ); ?></p>
                                            <?php $tag_line = get_field( 'tag_line', $post->ID ); if( ! empty($tag_line) ) echo '<p>'. $tag_line .'</p>'; ?>
                                            <?php $read_more_url = get_field( 'read_more_url', $post->ID ); if( ! empty($read_more_url) ) echo sprintf( '<a href="%s" class="btn-large">Read More</a>', $read_more_url ); ?>
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                </div>




                            </li>
                            <?php

                            endwhile;

                            ?>
                        </ul>

                    </div>

                </div>
            </div>
        </div>
    </section>
    <?php

    endif;
    // eof - Get the slides for the home page

    // Restore the default post data
    wp_reset_postdata();

    ?>

    <?php

    $events_args = array(
        'post_type' =>   'event',
        'post_status' => 'publish',
        'posts_per_page' => 3,
        'meta_key'       => 'featured_order',
        'orderby'        => 'meta_value_num',
        'order'          => 'ASC',
    );

    $events_args['meta_query'] = array(
        array(
            'key' => 'feature_on_homepage',
            'value' => 'Yes'
        )
    );

    $events_query = new WP_Query( $events_args );

    if( $events_query->have_posts() ) :

    ?>
    <section class="whats-on">
        <div class="container">

            <div class="row">
                <h2>What's on</h2>





                    <?php

                    while ( $events_query->have_posts() ) :

                        global $post;
                        $events_query->the_post();

                        $event_type = wp_get_post_terms( $post->ID, 'event_types' );
                        $start_date_raw = get_field( 'start_date', $post->ID );
                        $end_date_raw = get_field( 'end_date', $post->ID );

                        $duration_dates = WhatsOn::format_duration($start_date_raw, $end_date_raw, 'home');

                        ?>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="event-block">

                            <?php echo get_the_post_thumbnail( $post->ID, 'hero-event-listing' ); ?>

                            <div class="event-body">

                                <?php if( isset($event_type[0]) && is_object($event_type[0]) ) echo '<span>'. strtoupper($event_type[0]->name) .'</span>'; ?>

                                <h3><?php echo strtoupper(get_the_title() ); ?></h3>

                                <?php $excerpt_content = get_the_excerpt(); ?>

                                <?php if(isset($excerpt_content['100'])): ?>

                                    <?php $excerpt_content = substr($excerpt_content,0,100) . "..."; ?>

                                <?php endif; ?>

                                <p><?php echo $excerpt_content; ?></p>

                                <p><?php echo $duration_dates; ?></p>

                                <a href="<?php echo get_permalink($post->ID); ?>" class="btn-large">Read More</a>

                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>


            </div>
        </div>

    </section>
    <?php endif; wp_reset_postdata(); ?>


    <?php

    if( get_field('use_video') == 'Yes' ) :

    ?>
    <section class="media-section">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">

                    <div class="col-xs-12 col-sm-12 col-md-6 media-body child">
                      <div >
                        <h3><?php echo strtoupper( get_field( 'title' ) ); ?></h3>

                        <p><?php echo strip_tags( get_field( 'teaser' ) ); ?></p>

                        <a href="<?php echo get_field( 'read_more_url' ); ?>" class="btn-large" target="_blank"><?php echo strip_tags( get_field( 'read_more_label' ) ); ?></a>
                      </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-6">
                        <div class="videoWrapper">
                            <iframe width="480" height="360" src="//www.youtube.com/embed/<?php echo get_field( 'youtube_code' ); ?>" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <section class="contact-details">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 light">

                    <h2>Visit koorie heritage trust</h2>

                    <div class="col-xs-12 col-md-4 col-md-offset-1 info-block">

                        <h4>Where We Are</h4>

                        <p class="thin"><?php echo get_option('options_street_address'); ?></p>

                        <p><?php echo get_option('options_accessibility_notice'); ?></p>

                        <a href="<?php echo get_option('options_get_directions_url'); ?>" class="btn-large" target="_blank">Get directions</a>

                    </div>

                    <div class="col-xs-12 col-md-4 col-md-offset-2 info-block">

                        <h4>We Are Open</h4>

                        <p class="thin"><?php echo get_option('options_opening_hours'); ?></p>

                        <p><?php echo get_option('options_days_closed'); ?></p>

                        <?php echo \artsvicSupplemental\Buttons::contact_link_button(array('label' => 'Contact', 'css' => 'btn-large') ); ?>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 map">

                        <?php $location = get_field('koorie_location'); if($location) : //echo $location['address']; //  ?>


                        <iframe width="600" height="450" frameborder="0" style="border:0"
                                src="https://www.google.com/maps/embed/v1/place?q=<?php echo str_replace(' ', '%20', $location['address']); ?>&key=AIzaSyB3cBHI8dR5h3BYyTI5uWCe7SXRhvHY5go">
                        </iframe>
                        <?php endif; ?>

                    </div>

                </div>

            </div>
        </div>
    </section>

<?php ThemeTemplateUtilities::get_partial( 'scrollup' ); ?>

</div>

<?php get_footer(); ?>
