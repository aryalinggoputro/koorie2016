<?php
/**
 * @package koorie
 */

get_header();

// Toggle the subnav sidebar
ThemeTemplateUtilities::set_sidebar_usage( 'subnav' );

// Get the page title to be used throughout the mark up
$page_title = 'Search Results';

?>

    <div id="main" role="main">

        <?php ThemeTemplateUtilities::get_partial( 'subpage-heading-mobile', array('page_title' => $page_title) ); ?>

        <div class="container">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 ">

                    <?php ThemeTemplateUtilities::get_partial( 'subpage-heading-desktop', array('page_title' => $page_title) ); ?>


                    <form role="search" method="get" id="searchformpage" action="<?php echo esc_url( home_url( '/' ) ); ?>" class="search-lg hidden-xs hidden-sm">
                        <input type="text" aria-label="site search" name="s" placeholder="Search" value="<?php echo get_search_query(); ?>">
                        <button type="submit" class="btn-red">Submit</button>
                    </form>

                    <p class="num-results"><?php echo sprintf( __( 'Found <span>%s</span> results for <span>%s</span>' ), number_format($wp_query->found_posts), get_search_query() ); ?></p>

                    <?php if( have_posts() ) : ?>
                    <ol class="search-results">

                        <?php while( have_posts() ) : the_post(); ?>
                        <li id="result-post-<?php the_ID(); ?>">
                            <a href="<?php the_permalink(); ?>"  title="<?php the_title(); ?>"><?php the_title(); ?></a>

                            <p><?php
                                $find = $_GET['s'];
                                $txt = ThemeTemplateUtilities::excerpt_content(400);
                                $txt = strip_tags($txt);

                                echo str_replace($find,'<span class="highlight">'.$find.'</span>', $txt);
                                ?></p>
                        </li>
                        <?php endwhile; ?>

                    </ol>

                    <div class="page-pagination text-center">
                    <?php

                        // Previous/next page navigation.
                        the_posts_pagination( array(
                            'prev_text'          => __( 'Previous page' ),
                            'next_text'          => __( 'Next page' ),
                            'before_page_number' => '',
                            'screen_reader_text' => ' '
                        ) );

                    ?>
                    </div>

                    <?php endif; ?>

                </div>
            </div>

        </div>


        <?php ThemeTemplateUtilities::get_partial( 'nested-scrollup' ); ?>

    </div>

<?php get_footer(); ?>
