<?php
/**
 * @package koorie
 */
?>


</div> <!-- End of Wrapper  -->


<footer id="bottom" role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="footer-intro">
                    <p><?php echo get_option('options_respectful_acknowledgement'); ?></p>
                </div>
            </div>
        </div>
    </div>

    <section class="dark-grey">
        <div class="container">
            <div class="row">

                <div class="col-xs-12">

                    <ul class="partners">
                        <li><img src="<?php echo THEME_URL; ?>/assets/img/arts_victoria_logo.png" alt="Arts Victoria"></li>
                        <li><img src="<?php echo THEME_URL; ?>/assets/img/ministry_for_the_arts_ics_logo.png" alt="ministry for the arts ics"></li>
                        <li><img src="<?php echo THEME_URL; ?>/assets/img/ministry_for_the_arts_ivais_logo.png" alt="ministry for the arts ivais"></li>
                        <li><img src="<?php echo THEME_URL; ?>/assets/img/museums_australia_accreditation_logo.png" alt="museums australia accreditation"></li>
                        <li><img src="<?php echo THEME_URL; ?>/assets/img/City-of-Melbourne-Logo-NEW.png" alt="City of melbourne"></li>

                    </ul>

                </div>

            </div>
        </div>
    </section>

    <section class="light-grey services">

        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <section class="col-xs-12 col-sm-12 col-md-6">
                        <h4>General Enquiries</h4>
                        <span><?php echo get_option('options_general_enquiries_phone'); ?></span>
                    </section>
                    <section class="col-xs-12 col-sm-12 col-md-6">
                        <?php echo \artsvicSupplemental\Buttons::contact_link_button(array('label' => 'Enquiry', 'css' => 'btn-red btn-round' ) ); ?>
                        <p class="small"><?php echo get_option('options_privacy_notice'); ?></p>
                    </section>
                </div>
            </div>
        </div>

    </section>

    <section class="footer-links">

        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-6">
                    <ul>
                        <?php $footer_nav = new ThemeNavMenu( 'footer-navigation' ); if( is_array( $footer_nav->menu_items ) ) : ?>

                            <?php foreach($footer_nav->menu_items as $menu_item) : ?>
                                <li><a href="<?php echo $menu_item->url; ?>"><?php echo $menu_item->title; ?></a></li>
                            <?php endforeach; ?>

                        <?php endif; ?>

                    </ul>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <span>&#169; Koorie Heritage Trust Inc</span>
                </div>

            </div>
        </div>

    </section>

</footer>

<?php if( get_option( 'options_use_warning_popup' ) == 'On' ) : ?>
<section class="warning">

    <div class="container">
        <section class="top-section">
            <figure>
                <img src="<?php echo THEME_URL; ?>/assets/img/kht_flag_logo.png">
                <p>Koorie Heritage Trust Inc
                <blockquote>Gnokan Danna Murra Kor-ki</blockquote></p>
            </figure>

        </section>
        <section class="main-section">

            <h2><?php echo get_option( 'options_warning_heading' ); ?></h2>

            <?php echo get_option( 'options_warning_text' ); ?>

            <form>
                <fieldset>
                    <input name="readWarning" id="readWarning" type="checkbox">
                    <label for="readWarning">I have read and agree.</label>
                    <span class="validation-message">Please agree before clicking enter</span>
                </fieldset>
                <button id="warning-btn" class="btn-warning">Enter</button>
            </form>
        </section>
    </div>

</section>
<?php endif; ?>

<!--
####################################
End of site Warning
####################################
-->


<?php
  wp_footer();
?>

</body>
</html>
