<?php
/**
 * Template Name: Image Gallery
 * Description: Used to display the image galleries
 *
 * @package koorie
 */

get_header();

// Toggle the subnav sidebar
ThemeTemplateUtilities::set_sidebar_usage( 'subnav' );

// Get the page title to be used throughout the mark up
$page_title = get_the_title();

?>
    <div id="main" role="main">

    <?php ThemeTemplateUtilities::get_partial( 'subpage-heading-mobile', array('page_title' => $page_title) ); ?>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">

            <?php ThemeTemplateUtilities::get_partial( 'subpage-heading-desktop', array('page_title' => $page_title) ); ?>

                <div class="row">

                    <?php get_sidebar(); ?>

                <section class="col-xs-12 col-md-9">

                <div class="row gallery-container">

                <?php

                $images = get_field('gallery-images');

                if( $images ) :

                    foreach( $images as $image ) :
                ?>
                <div class="col-xs-12 col-sm-6 col-md-6 ">
                        <figure class="gallery-item">
                            <img src="<?php echo $image['sizes']['medium']; ?>">
                            <figcaption>
                                <h6><?php echo $image['title']; ?></h6>

                                <?php echo $image['caption']; ?>
                            </figcaption>
                        </figure>
                </div>
                <?php endforeach; endif; ?>



                </div>

                </section>

                </div>
            </div>
        </div>
    </div>


    <?php ThemeTemplateUtilities::get_partial( 'nested-scrollup' ); ?>

    </div>
<?php

get_footer();

?>
