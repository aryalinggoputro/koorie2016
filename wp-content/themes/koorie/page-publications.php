<?php
/**
 * Template Name: Publications - Current
 * Description: Used to display the posts for 'Publications'
 *
 * @package koorie
 */

get_header();

// Toggle the whats-on sidebar
ThemeTemplateUtilities::set_sidebar_usage( 'whats-on' );

// Get the page title to be used throughout the mark up
$page_title = get_the_title();

$posts_per_page = get_option('whats_on_results_per_page');

// Set the date filter
if( ! isset( $date_filter ) )
{
    $date_filter = 'nowShowing';
}

?>
    <div id="main" role="main">

        <?php ThemeTemplateUtilities::get_partial( 'subpage-heading-mobile', array('page_title' => $page_title) ); ?>

        <div class="container">

            <div class="row">
                <div class="col-xs-12">

                    <?php ThemeTemplateUtilities::get_partial( 'subpage-heading-desktop', array('page_title' => $page_title) ); ?>

                    <div class="row">

                        <?php ThemeTemplateUtilities::get_partial( 'whats-on', array( 'date_filter' => $date_filter ) ); ?>

                        <section class="col-xs-12 col-sm-12 col-md-9">

                            <?php

                            // Get the current page
                            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

                            $pub_args = array(
                                'post_type' =>   'publications',
                                'post_status' => 'publish',
                                'posts_per_page' => $posts_per_page,
                                'paged'          => $paged,
                                'order'          => 'DESC',
                            );

                            $events_query = new WP_Query( $pub_args );

                            while ( $events_query->have_posts() ) :

                                global $post;
                                $events_query->the_post();

                                get_template_part('content', 'publications');

                            endwhile;

                            ?>

                            <div class="page-pagination text-center">
                            <?php

                            $big = 999999999; // need an unlikely integer

                            echo paginate_links( array(
                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                'format' => '?paged=%#%',
                                'current' => max( 1, get_query_var('paged') ),
                                'total' => $events_query->max_num_pages
                            ) );

                            ?>
                            </div>

                            <?php

                            wp_reset_postdata();

                            ?>
                        </section>
                    </div>

                </div>
            </div>

        <?php ThemeTemplateUtilities::get_partial( 'nested-scrollup' ); ?>

    </div>
  </div>

<?php get_footer(); ?>
