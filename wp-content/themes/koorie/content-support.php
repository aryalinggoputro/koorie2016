<?php
/**
 * @package koorie
 */

global $post;

$dontation_button = ( get_field('donation_button', $post->ID) == 'Yes' ) ? true : false;
$sub_title = get_field('sub_title', $post->ID);

?>


                        <article class="titled-article<?php if($dontation_button) echo '  donation'; ?>">
                            <a id="<?php echo $post->post_name; ?>"></a>
                            <h2 class="light"><?php the_title(); echo ($sub_title != '') ? ' <br>'.$sub_title : ''; ?></h2>
                            <?php if($dontation_button) echo sprintf('<a href="%s" class="yellow-btn btn-round donate">Donation?</a>', get_option('options_donate_url') ); ?>

                            <div class="media">
                                <div class="media-body">

                                    <?php

                                    the_content();

                                    if( get_field('document_download', $post->ID) ) :

                                        $attachment_doc = get_field('document_download', $post->ID);

                                        if($attachment_doc) :
                                            $url = $attachment_doc['url'];
                                            $title = $attachment_doc['title'];
                                            $mime = $attachment_doc['mime_type'];
                                            $file_path = str_replace(get_site_url(), rtrim(ABSPATH, '/'), $url);
                                            ?>
                                            <a href="<?php echo $url; ?>" class="dark icon-link"><i class="fa fa-file-pdf-o"></i><?php echo $title; echo ($mime == 'application/pdf') ? ' (PDF) ' : ''; echo ' '. strtoupper(ThemeTemplateUtilities::filesize_formatted($file_path) ); ?></a>
                                        <?php endif; endif; ?>

                                </div>

                                <?php if ( has_post_thumbnail() ) :

                                    $img_id = get_post_thumbnail_id();

                                    ?>
                                    <div class="media-right">
                                        <figure>
                                            <?php echo get_the_post_thumbnail( $post->ID, 'post-feature-right-size' );

                                            $feature_args = array( 'post_type' => 'attachment', 'orderby' => 'menu_order', 'order' => 'ASC', 'post_mime_type' => 'image' ,'post_status' => null, 'numberposts' => null, 'post_parent' => $post->ID );

                                            $attachments = get_posts($feature_args);
                                            $image_caption = '';

                                            if ( ! empty($attachments) )
                                            {
                                                $attachment = $attachments[0];
                                                $image_title = $attachment->post_title;
                                                $caption = $attachment->post_excerpt;
                                                $description = $image->post_content;

                                                $image_caption = sprintf('<figcaption>Image: %s</figcaption>', $caption);
                                            }

                                            ?>

                                            <?php echo $image_caption; ?>
                                        </figure>
                                    </div>
                                <?php endif; ?>

                            </div>
                        </article>