<?php
/**
 * Template Name: Work for Us
 * Description: Displays available job positions at KHT.
 *
 * @package koorie
 */

get_header();

// Toggle the subnav sidebar
ThemeTemplateUtilities::set_sidebar_usage( 'subnav' );

// Get the page title to be used throughout the mark up
$page_title = get_the_title();
global $post;
$page_id = $post->ID;

?>
<div id="main" role="main">

    <?php ThemeTemplateUtilities::get_partial( 'subpage-heading-mobile', array('page_title' => $page_title) ); ?>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                <?php ThemeTemplateUtilities::get_partial( 'subpage-heading-desktop', array('page_title' => $page_title) ); ?>

                <div class="row">

                    <?php get_sidebar(); ?>

                    <section class="col-xs-12 col-sm-12 col-md-9">

                    <?php

                    $jobs_query_args = array(
                        'post_type' => 'position_available',
                        'post_status' => 'publish',
                        'meta_key'       => 'position_order',
                        'orderby'        => 'meta_value_num',
                        'order'          => 'ASC'
                    );

                    $jobs_query = new WP_Query( $jobs_query_args );

                    if( $jobs_query->have_posts() ) :

                        while( $jobs_query->have_posts() ) :

                            $jobs_query->the_post();
                            global $post;
                    ?>

                    <?php ThemeTemplateUtilities::get_partial( 'work-for-us' ); ?>

                        <?php endwhile; ?>

                    <?php else : ?>
                        <article class="job-opening">
                            <p><?php echo get_field('wfu_no_listings_notice', $page_id); ?></p>
                        </article>
                    <?php endif; wp_reset_postdata(); ?>

                    </section>
                </div>

            </div>
        </div>
    </div>

    <?php ThemeTemplateUtilities::get_partial( 'nested-scrollup' ); ?>

</div>
<?php get_footer(); ?>
