<?php
/**
 * @package koorie
 */
?>
<!DOCTYPE html>

<!--[if IE 8]><html class="lt-ie10 lt-ie9 ie8" lang="en"><![endif]-->
<!--[if IE 9]><html lass="lt-ie10 ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?php echo get_option('options_site_title'); ?></title>

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Stylesheets -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?php echo THEME_URL; ?>/assets/build/css/screen.min.css">

    <?php wp_head(); ?>

    <!--[if lt IE 9]>
    <script src="<?php echo THEME_URL; ?>/assets/js/lib/html5.min.js"></script>
    <script src="<?php echo THEME_URL; ?>/assets/js/lib/respond.min.js"></script>
    <![endif]-->

</head>

<body<?php $theme_choice = get_option('options_site_colour_theme'); echo ( $theme_choice == 'Light' ) ? ' class="bg-white"' : '' ?>>

<div class="wrapper" id="top">

<header role="banner">
     <div class="container-fluid">

        <div class="row stripe-bg">
            <div class="container">

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                      <div class="row">

                        <div class="logo col-md-4">
                            <a href="/">
                                <img src="<?php echo THEME_URL; ?>/assets/img/logo.png" alt="logo handshake">

                                <p>Koorie heritage trust<span class="small-p">WOMINJEKA | WELCOME</span></p>
                            </a>
                        </div>

                        <div class="col-md-8 ">

                            <div class="col-md-12 hidden-xs hidden-sm">

                                <nav class="top-nav">
                                    <ul>

                                    <?php $top_nav = new ThemeNavMenu( 'top-navigation' ); if( is_array( $top_nav->menu_items ) ) : ?>

                                        <?php foreach($top_nav->menu_items as $menu_item) : ?>
                                            <li><a href="<?php echo $menu_item->url; ?>"><?php echo $menu_item->title; ?></a></li>
                                        <?php endforeach; ?>

                                    <?php endif; ?>
                                        <li>
                                            <form role="search" method="get" id="searchform" class="search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                                                <input type="text" name="s" placeholder="Search" value="<?php echo get_search_query(); ?>">
                                                <button type="submit"><i class="fa fa-search"></i></button>
                                            </form>
                                        </li>
                                    </ul>
                                </nav>

                            </div>

                            <div class="col-md-12  social-links">
                                <div class="social-icons hidden-xs hidden-sm">
                                    <?php if(get_option('display_facebook_icon', 'options')) : ?>
                                    <a href="<?php echo get_option('options_facebook_url'); ?>" target="_blank">
                                      <img width="35" height="35" src="<?php echo THEME_URL; ?>/assets/img/facebook_circle_color-512.png">
                                    </a>
                                    <?php endif;
                                    if(get_option('display_twitter_icon', 'options')) :
                                    ?>

                                    <a href="<?php echo get_option('options_twitter_url'); ?>" target="_blank">
                                      <img width="35" height="35" src="<?php echo THEME_URL; ?>/assets/img/twitter_circle_color-512.png">
                                    </a>
                                    <?php endif;   if(get_option('display_instagram_icon', 'options')) :
                                        ?>

                                        <a href="<?php echo get_option('options_instagram_url'); ?>" target="_blank">
                                            <img width="35" height="35" src="<?php echo THEME_URL; ?>/assets/img/instagram_circle.png">
                                        </a>
                                    <?php endif; ?>
                                </div>
                                <a href="<?php echo get_option('options_donate_url'); ?>" class="yellow-btn btn-round">Donate</a>
                            </div>

                        </div>
                    </div>
                  </div>

                </div>

            </div>
        </div>

        <div class="row light-header sticky">
            <div class="container">
                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12 ">

                        <div class="site-toggle hidden-md"><span></span></div>

                        <?php ThemeTemplateUtilities::the_main_menu(); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</header>
