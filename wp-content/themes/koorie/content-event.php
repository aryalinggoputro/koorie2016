<?php
/**
 * @package koorie
 */

global $post;
global $paged;

$event_type = wp_get_post_terms( $post->ID, 'event_types' );

// DATE FORMATTING --> Get and format the start and end dates for event
$start_date_raw = get_field( 'start_date', $post->ID );
$end_date_raw = get_field( 'end_date', $post->ID );

$duration_dates = WhatsOn::format_duration($start_date_raw, $end_date_raw);

?>

                            <article class="content-feature-event">
                                <div class="media-left">
                                    <?php echo get_the_post_thumbnail( $post->ID, 'hero-event-listing' ); ?>
                                </div>

                                <div class="media-body">
                                    <?php if( isset($event_type[0]) && is_object($event_type[0]) ) echo '<h6>'. strtoupper($event_type[0]->name) .'</h6>'; ?>
                                    <h4><?php echo strtoupper(get_the_title() ); ?></h4>

                                    <?php echo $duration_dates; ?>

                                    <p><?php echo strip_tags(get_the_excerpt()); ?></p>
                                    <?php

                                    $link = get_permalink( $post->ID );
                                    $link .= ( isset($paged) && ($paged > 0) ) ? '?epage='.$paged : '';

                                    ?>
                                    <a href="<?php echo $link; ?>" class="btn-large">Read More</a>
                                </div>

                            </article>
