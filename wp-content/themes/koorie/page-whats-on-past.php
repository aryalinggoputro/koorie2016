<?php
/**
 * Template Name: What's On - Past
 * Description: Displays events that have passed their running date.
 *
 * @package koorie
 */

$date_filter = 'pastExhibitions';

include_once('page-whats-on.php');