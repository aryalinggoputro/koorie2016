<?php
/**
 * @package koorie
 */

get_header(); ?>

<div id="main" role="main">
    <?php ThemeTemplateUtilities::get_partial( 'subpage-heading-mobile', array('page_title' => $page_title) ); ?>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                <?php ThemeTemplateUtilities::get_partial( 'subpage-heading-desktop', array('page_title' => $page_title) ); ?>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <?php

                        if( have_posts() ) :

                            while ( have_posts() ) :

                                the_post();
                                global $post;

                                get_template_part('content');

                            endwhile;

                          else : ?>
                            <article class="titled-article">

                                <div class="media">
                                    <div class="media-body">

                                        <p>Sorry, there isn't any information currently available.</p>

                                    </div>

                                </div>
                            </article>
                        <?php endif; wp_reset_postdata(); ?>


                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php ThemeTemplateUtilities::get_partial( 'nested-scrollup' ); ?>
</div>

<?php get_footer(); ?>
