<?php
/**
 * Class ThemeTemplateUtilities
 *
 * A helper / factory class to support the various features
 * of the KHT theme.
 *
 * @package koorie
 */

class ThemeTemplateUtilities {

    public static $sidebar_usage;

    /**
     * get_page_template
     *
     * Shortcut method for including the desired template.
     *
     * @param $slug
     * @return void
     */
    public static function get_page_template( $slug )
    {
        self::include_file( THEME_DIR .'/templates/'. $slug .'.php' );
    }

    /**
     * get_partial
     *
     * Used to get the desired partial template.
     *
     * @param $slug
     * @return void
     */
    public static function get_partial( $slug, $args = array() )
    {
        self::include_file( THEME_DIR .'/partials/'. $slug .'.php', $args );
    }

    /**
     * include_file
     *
     * Performs the include of requested script.
     *
     * @param $file_path
     * @return void
     */
    private static function include_file($file_path, $args = array() )
    {
        if( file_exists($file_path) )
        {
            include( $file_path );
        }
    }

    /**
     * init_sidebar_usage
     *
     * Must be called in the 'functions.php' of the theme.
     *
     * @return void
     */
    public static function init_sidebar_usage()
    {
        self::$sidebar_usage = array(
            'subnav' => false,
            'whats-on' => false,
            'call-to-action' => false
        );
    }

    /**
     * set_sidebar_usage
     *
     * Toggles which sidebar is compatible. Can take either
     * an array or a string to set multiple or a single use.
     * If just a string, the identified sidebar
     * will be set to true.
     *
     * @param $args
     * @return void
     */
    public static function set_sidebar_usage( $args )
    {
        if( ! empty( $args ) )
        {
            if( is_array( $args ) )
            {
                foreach($args as $key => $bool)
                {
                    self::$sidebar_usage[$key] = true;
                }

            // Supplied a string
            } else {

                self::$sidebar_usage[$args] = true;
            }
        }
    }

    /**
     * check_sidebar_usage
     *
     * Used to toggle a sidebar to render.
     *
     * @param $args
     * @return bool
     */
    public static function check_sidebar_usage( $sidebar )
    {
        if( ! empty( $sidebar ) )
        {
            if( count( self::$sidebar_usage ) > 0 )
            {
                foreach(self::$sidebar_usage as $key => $bool)
                {
                    if( ($key == $sidebar) && ($bool == true) )
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * clean_input
     *
     * @param $input
     * @return mixed
     */
    public static function clean_input( $input )
    {
        $search = array(
            '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
            '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
            '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
            '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
        );

        $output = preg_replace($search, '', $input);

        return $output;
    }

    /**
     * sanitize
     *
     * @param $input
     * @return string
     */
    public static function sanitize($input)
    {
        $output = $input;

        if ( is_array( $input ) )
        {
            foreach($input as $var=>$val)
            {
                $output[$var] = self::sanitize( $val );
            }
        }
        else {

            if ( get_magic_quotes_gpc() )
            {
                $input = stripslashes( $input );
            }

            $input  = self::clean_input( $input );
            $output = mysql_real_escape_string( $input );
        }

        return $output;
    }

    /**
     * filesize_formatted
     *
     * @param $file
     * @return string
     */
    public static function filesize_formatted($file)
    {
        $bytes = filesize($file);

        if ($bytes >= 1073741824) {
            return number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            return number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            return number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            return $bytes . ' bytes';
        } elseif ($bytes == 1) {
            return '1 byte';
        } else {
            return '0 bytes';
        }
    }

    /**
     * excerpt_content
     *
     * @param string $length_callback
     * @return mixed|string|void
     */
    public static function excerpt_content( $length_callback = '' )
    {
        global $post;

        if ( has_excerpt() )
        {
            $output = get_the_excerpt();
            $output = apply_filters('wptexturize', $output);
            $output = apply_filters('convert_chars', $output);

            if($length_callback)
            {
                $output =  substr($output, 0, ($length_callback-3));
                $output = $output.'...';
            }

            $output = '<div class="article-excerpt"><p>' . strip_tags($output) . '</p></div>';

        } else {

            $output = get_the_content();
            $output = apply_filters('wptexturize', $output);
            $output = apply_filters('convert_chars', $output);

            if($length_callback)
            {
                $output =  substr($output, 0, ($length_callback-3));
                $output = $output.'...';
            }

            $output = '<div class="article-excerpt"><p>' . strip_tags($output) . '</p></div>';
        }

        return $output;
    }

    /**
     * get_breadcrumbs
     *
     * @return Breadcrumbs
     */
    public static function get_breadcrumbs()
    {
        return new Breadcrumbs();
    }

    /**
     * set_main_menu_items
     *
     * @param $menu_items
     * @return void
     */
    public static function set_main_menu_items( $menu_items )
    {
        wp_cache_add( 'menu_items', $menu_items, 'main_navigation' );
    }

    /**
     * get_main_menu_items
     *
     * @return mixed
     */
    public static function get_main_menu_items()
    {
        return wp_cache_get( 'menu_items', 'main_navigation' );
    }

    public static function get_main_menu_item_children( $item_id )
    {
        $koorie_main_menu_items = self::get_main_menu_items();

        foreach($koorie_main_menu_items as $menu_item)
        {
            if($item_id == $menu_item->object_id)
            {
                if( isset($menu_item->children) && ( count($menu_item->children) > 0 ) )
                {
                    return $menu_item->children;
                }
            }
        }

        return array();
    }

    /**
     * the_main_menu
     * @return void
     */
    public static function the_main_menu()
    {
        $menu = new MainMenu();
        $menu->show();
    }
}