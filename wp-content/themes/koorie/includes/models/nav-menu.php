<?php

/**
 * Class ThemeNavMenu
 */

class ThemeNavMenu {

    // Singleton for later reference
    public static
        $menus = array(),
        $queried_object,
        $active_menu_item = false,
        $preceding_hierarchy = array(),
        $substitute_wp_query;

    public
        $menu_location,
        $menu_items;

    /**
     * @param string $menu_location
     */
    public function __construct( $menu_location, $substitute_wp_query = false )
    {
        // Set the substitute query if provided
        if( $substitute_wp_query )
        {
            if( is_a( $substitute_wp_query, 'WP_Query' ) )
            {
                self::$substitute_wp_query = $substitute_wp_query;
            }
        }

        $this->get_instance_queried_object();
        $this->menu_location = $menu_location;

        if( ! empty( $menu_location ) && is_string( $menu_location ) )
        {
            // Check if menu is in list with active states applied
            if( isset( self::$menus[$menu_location] ) && ! empty( self::$menus[$menu_location] ) )
            {
                $this->menu_items = self::$menus[$menu_location];

            // Attempt to get menu items from cache
            } else {
                
                // Not available in cache, query WP and process for the menu items
                if( ! $this->retrieve_menu_items_from_cache() )
                {
                    $this->get_menu_items();
                }
            }
        }
    }

    /**
     * retrieve_menu_items_from_cache
     * 
     * Attempts to get the menu items for this menu's location from cache
     * 
     * @return bool
     */
    private function retrieve_menu_items_from_cache()
    {
        $applied_result = self::apply_active_states( self::get_menu_items_from_cache( $this->menu_location ) );
        $this->menu_items = $applied_result->menu_items;
        self::$menus[ $this->menu_location ] = $this->menu_items;
        
        return ( ! empty( $this->menu_items ) ) ? true : false;
    }

    /**
     * set_menu_items_to_cache
     * 
     * @param string $menu_location
     * @param mixed $menu_items
     */
    private static function set_menu_items_to_cache( $menu_location, $menu_items )
    {
        wp_cache_add( 'nav_menu_items', $menu_items, $menu_location );
    }

    /**
     * get_menu_items_from_cache
     * 
     * @param $menu_location
     * @return bool|mixed
     */
    private static function get_menu_items_from_cache( $menu_location )
    {
        return wp_cache_get( 'nav_menu_items', $menu_location );
    }

    /**
     * add_menu_to_menus
     *
     * @param string $menu_location
     * @param mixed $menu
     */
    private static function add_menu_to_menus( $menu_location, $menu )
    {
        self::$menus[ $menu_location ] = $menu;
    }

    /**
     * get_menu_items
     */
    private function get_menu_items()
    {
        $menu_items_args = array(
            'order'                  => 'ASC',
            'orderby'                => 'menu_order',
            'post_type'              => 'nav_menu_item',
            'post_status'            => 'publish',
            'output'                 =>  OBJECT,
            'output_key'             => 'menu_order',
            'nopaging'               => true,
            'update_post_term_cache' => false );

        $menu_items = wp_get_nav_menu_items( $this->menu_location, $menu_items_args );

        if( ! empty( $menu_items ) )
        {
            $final_menu_items = array();

            array_walk( $menu_items, function( &$menu_item ) use( &$final_menu_items ) {

                $menu_item->use_active_class = false;
                $menu_item->classes_string = '';

                if( ! empty($menu_item->classes) )
                {
                    for($i=0;$i<count($menu_item->classes);$i++)
                    {
                        if($i > 0) $menu_item->classes_string .= ' ';

                        $menu_item->classes_string .= $menu_item->classes[$i];
                    }
                }

                if( $menu_item->menu_item_parent > 0 )
                {
                    if( count( $final_menu_items ) > 0 )
                    {
                        for($i=0;$i<count($final_menu_items);$i++)
                        {
                            if($menu_item->menu_item_parent == $final_menu_items[$i]->ID)
                            {
                                if( isset($final_menu_items[$i]->children) )
                                {
                                    $final_menu_items[$i]->children[] = $menu_item;

                                } else {

                                    $final_menu_items[$i]->children = array( $menu_item );
                                }
                            }
                        }
                    }

                } else {

                    $final_menu_items[] = $menu_item;
                }
            });

            // Set the cache and get 
            self::set_menu_items_to_cache( $this->menu_location, $final_menu_items );

            // Apply active states
            $applied_result = self::apply_active_states( $final_menu_items );
            $final_menu_items = $applied_result->menu_items;

            // Set the singleton element
            self::$menus[ $this->menu_location ] = $final_menu_items;

            // Finally set this instance's menu items
            $this->menu_items = $final_menu_items;
        }
    }

    /**
     * apply_active_states
     *
     * A recursive walker to apply active classes within hierarchy
     *
     * @param mixed $menu_items
     * @return stdClass
     */
    public static function apply_active_states( $menu_items )
    {
        $result = new stdClass();
        $result->applied = false;
        $result->menu_items = $menu_items;
        $queried_object = self::get_queried_object();

        array_walk( $result->menu_items, function( &$menu_item ) use( $queried_object, &$result ) {

            $set_current_active = false;

            // Match up the menu item to the current queried object
            if( ( $menu_item->object_id == $queried_object->object_id ) &&
                ( $menu_item->object    == $queried_object->object )    &&
                ( $menu_item->type      == $queried_object->type ) )
            {
                $menu_item->use_active_class = true;
                $result->applied = true;
                $set_current_active = true;

            // Due to the fact that WP doesn't have a special identifier for a menu item that
            // identifies the home page, we substitute this with a custom type and URL of '/'.
            // TODO: Refactor this later for a better solution and a multi-site compatible one
            } elseif( ( $menu_item->type == 'custom' ) &&
                      ( $menu_item->url == '/' )       &&
                      ( $queried_object->object == 'home-link' ) )
            {
                $menu_item->use_active_class = true;
                $result->applied = true;
                $set_current_active = true;

            // Move on to check for children
            } else {

                if( isset( $menu_item->children )   &&
                    is_array( $menu_item->children) &&
                    ( count( $menu_item->children ) > 0 )
                  )
                {
                    $child_result = ThemeNavMenu::apply_active_states( $menu_item->children );

                    if( $child_result->applied == true )
                    {
                        $result->applied = true;
                        $menu_item->use_active_class = true;
                        $menu_item->activeClass = 'active';
                        $menu_item->children = $child_result->menu_items;
                        ThemeNavMenu::$preceding_hierarchy[] = $menu_item;

                    } else {

                        $menu_item->use_active_class = false;
                        $menu_item->activeClass = '';
                    }
                }
            }

            // To be used for quick reference later
            if( $set_current_active )
            {
                ThemeNavMenu::$active_menu_item = $menu_item;
            }

        });

        return $result;
    }

    /**
     * get_queried_object
     *
     * @return mixed
     */
    public static function get_queried_object()
    {
        if( self::$substitute_wp_query )
        {
            $wp_query = self::$substitute_wp_query;
            $queried_object = self::$substitute_wp_query->post;

        } else {

            global $wp_query;
            $queried_object = $wp_query->get_queried_object();
        }

        if( empty( self::$queried_object ) )
        {
            // Set initial values
            self::$queried_object = new stdClass();
            self::$queried_object->object_id = 0;
            self::$queried_object->object = 'custom';
            self::$queried_object->type = 'custom';

            if( ! empty( $queried_object ) && is_object( $queried_object ) )
            {
                //
                switch( get_class( $queried_object ) )
                {
                    case 'WP_Post':

                        self::$queried_object->object_id = $queried_object->ID;
                        self::$queried_object->object = $queried_object->post_type;
                        self::$queried_object->type = 'post_type';

                        break;

                    case 'stdClass':

                        if( isset( $queried_object->term_id ) ) {

                            self::$queried_object->object_id = $queried_object->term_id;
                            self::$queried_object->object = $queried_object->slug;
                            self::$queried_object->type = 'taxonomy';
                        }
                }

            } else {

                if( is_home() || is_front_page() )
                {
                    self::$queried_object->object = 'home-link';
                }
            }
        }

        return self::$queried_object;
    }

    /**
     * get_instance_queried_object
     */
    private function get_instance_queried_object()
    {
        if( empty( self::$queried_object ) )
        {
            self::$queried_object = self::get_queried_object();
        }
    }

    /**
     * current_has_children
     *
     * Check if the currently viewed page has any children in the menu
     *
     * @return bool
     */
    public static function current_has_children()
    {
        if( self::$active_menu_item )
        {
            if( isset( self::$active_menu_item->children ) &&
                is_array( self::$active_menu_item->children ) &&
                ! empty( self::$active_menu_item ) )
            {
                return true;
            }
        }

        return false;
    }

    /**
     * get_current_item
     *
     * @return bool
     */
    public static function get_current_item()
    {
        return ( ! empty( self::$active_menu_item ) ) ? self::$active_menu_item : false;
    }

    /**
     * get_current_children
     *
     * @return array
     */
    public static function get_current_children()
    {
        return ( self::current_has_children() ) ? self::$active_menu_item->children : array();
    }

    /**
     * current_is_child
     *
     * @return bool
     */
    public static function current_is_child()
    {
        if( isset( self::$active_menu_item ) && self::$active_menu_item->menu_item_parent > 0 )
        {
            return true;
        }

        return false;
    }

    /**
     * get_top_children
     *
     * @return array
     */
    public static function get_top_children()
    {
        return ( isset( self::$preceding_hierarchy[0] ) ) ? self::$preceding_hierarchy[0]->children : array();
    }

    /**
     * get_top_parent
     *
     * @return mixed|bool
     */
    public static function get_top_parent()
    {
        return ( isset( self::$preceding_hierarchy[0] ) ) ? self::$preceding_hierarchy[0] : false;
    }
}