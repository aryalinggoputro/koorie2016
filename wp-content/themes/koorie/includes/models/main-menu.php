<?php

/**
 * Class MainMenu
 *
 * @package koorie
 */

class MainMenu {

    public $items = array();

    public function __construct()
    {
        $menu_items_args = array(
            'order'                  => 'ASC',
            'orderby'                => 'menu_order',
            'post_type'              => 'nav_menu_item',
            'post_status'            => 'publish',
            'output'                 => OBJECT,
            'output_key'             => 'menu_order',
            'nopaging'               => true,
            'update_post_term_cache' => false );

        $menu_items = wp_get_nav_menu_items( 'header-navigation', $menu_items_args );

        $final_menu_items = array();

        array_walk( $menu_items, function( &$menu_item ) use( &$final_menu_items ) {

            if( $menu_item->menu_item_parent > 0 )
            {
                if( count( $final_menu_items ) > 0 )
                {
                    for($i=0;$i<count($final_menu_items);$i++)
                    {
                        if($menu_item->menu_item_parent == $final_menu_items[$i]->ID)
                        {
                            if( isset($final_menu_items[$i]->children) )
                            {
                                $final_menu_items[$i]->children[] = $menu_item;

                            } else {

                                $final_menu_items[$i]->children = array( $menu_item );
                            }
                        }
                    }
                }

            } else {

                $final_menu_items[] = $menu_item;
            }
        });

        $this->items = $final_menu_items;
        ThemeTemplateUtilities::set_main_menu_items( $final_menu_items );
    }

    public function show()
    {
        if( ! empty( $this->items ) )
        {
            ThemeTemplateUtilities::get_partial('main-navigation', array('menu_items' => $this->items) );
        }
    }
}