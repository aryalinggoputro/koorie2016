<?php

/**
 * Class Breadcrumbs
 *
 * @package koorie
 */

class Breadcrumbs {

    public $crumbs = array();  // url | label
    public $back_url;

    public function add_crumb($url, $label)
    {
        $this->crumbs[] = array('url' => $url, 'label' => $label);
    }

    public function has_crumbs()
    {
        return (count($this->crumbs) > 0) ? true : false;
    }

    public function show()
    {
        if( ! empty( $this->crumbs ) )
        {
            ThemeTemplateUtilities::get_partial('breadcrumbs', array('breadcrumbs' => $this) );
        }
    }
}