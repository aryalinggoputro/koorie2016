<?php

/**
 * Class WhatsOn
 *
 * @package koorie
 */

class WhatsOn extends ThemeTemplateUtilities {

    /**
     * format_duration
     *
     * Used to format the event duration from the start and end dates.
     *
     * @param $start_date_raw
     * @param $end_date_raw
     * @param bool $for_home
     *
     * @return string
     */
    public static function format_duration($start_date_raw, $end_date_raw, $for = false)
    {
        $start_date = $date = DateTime::createFromFormat('Y-m-d', $start_date_raw);
        $end_date = $date = DateTime::createFromFormat('Y-m-d', $end_date_raw);

        $end_date_display = '';
        $duration_dates = '';

        if($start_date && $end_date)
        {

            if($for)
            {
                switch($for)
                {
                    case 'home' :

                        $start_date_display = $start_date->format('jS') .' '. $start_date->format('F');

                        if( date('Y') != $start_date->format('Y') )
                        {
                            $start_date_display .= ' '. $start_date->format('Y') .' ';
                        }

                        if($start_date_raw != $end_date_raw)
                        {
                            $end_date_display = 'to '. $end_date->format('jS') .' '. $end_date->format('F');

                            if( date('Y') != $end_date->format('Y') )
                            {
                                $end_date_display .= ' '. $end_date->format('Y');
                            }
                        }

                        $duration_dates =
                            sprintf( '%s %s', $start_date_display, $end_date_display );

                    break;

                    case 'detailed' :

                        $start_date_display = $start_date->format('jS') .' '. $start_date->format('F');

                        if( date('Y') != $start_date->format('Y') )
                        {
                            $start_date_display .= ' '. $start_date->format('Y') .' ';
                        }

                        if($start_date_raw != $end_date_raw)
                        {
                            $end_date_display = ' Until '. $end_date->format('jS') .' '. $end_date->format('F');

                            if( date('Y') != $end_date->format('Y') )
                            {
                                $end_date_display .= ' '. $end_date->format('Y');
                            }
                        }

                        $duration_dates =
                            sprintf( '<time>%s %s</time>', $start_date_display, $end_date_display );

                    break;
                }


            } else {

                $start_date_display = '<span class="start-date"><span>'.
                    $start_date->format('jS') .'</span> '. $start_date->format('F') .'</span>';


                if( date('Y') != $start_date->format('Y') )
                {
                    $start_date_display .= ' '. $start_date->format('Y') .' ';
                }

                if($start_date_raw != $end_date_raw)
                {
                    $end_date_display = 'Until '. $end_date->format('jS') .' '. $end_date->format('F');

                    if( date('Y') != $end_date->format('Y') )
                    {
                        $end_date_display .= ' '. $end_date->format('Y');
                    }
                }

                $duration_dates =
                    sprintf( '<time>%s %s</time>', $start_date_display, $end_date_display );
            }
        }

        return $duration_dates;
    }

    public static function get_future_months()
    {
        global $wpdb;

        $current_date = date('Y-m-d');
        $current_year = substr($current_date, 0, -6);
        $options = array();

        $event_months = $wpdb->get_results( "

            SELECT CAST(t1.meta_value AS DATE),
            DATE_FORMAT(t1.meta_value, '%M-%Y-%m') AS event_month_year
            FROM wp_postmeta t1
            LEFT JOIN wp_posts t2 ON t1.post_id = t2.ID
            WHERE t1.meta_key = 'start_date' AND t2.post_type = 'event' AND t1.meta_value > '".$current_date."'
            GROUP BY event_month_year
            ORDER BY t1.meta_value ASC;
        " );

        if( ! empty($event_months) )
        {
            $i = 0;

            foreach ( $event_months as $event_month )
            {
                $option_year = substr( $event_month->event_month_year, -7, strpos($event_month->event_month_year, '-'), -3);

                $options[$i] = array(
                    'value' => substr( $event_month->event_month_year, -7),
                    'label' => str_replace( substr( $event_month->event_month_year, -8), '', $event_month->event_month_year )
                );

                $options[$i]['label'] .= ( $current_year != $option_year ) ? ' '.$option_year : '';
                $i++;
            }
        }

        return $options;
    }

    public static function get_past_years()
    {
        global $wpdb;

        $current_date = date('Y-m-d');
        $options = array();

        $event_years = $wpdb->get_results( "

            SELECT CAST(t1.meta_value AS DATE), 
            DATE_FORMAT(t1.meta_value, '%Y') AS event_year
            FROM wp_postmeta t1 
            LEFT JOIN wp_posts t2 ON t1.post_id = t2.ID 
            WHERE t1.meta_key = 'end_date' AND t2.post_type = 'event' AND t1.meta_value < '".$current_date."'
            GROUP BY event_year 
            ORDER BY t1.meta_value DESC;
        " );

        if( ! empty($event_years) )
        {
            foreach ( $event_years as $event_year )
            {
                $options[] = array(
                    'value' => $event_year->event_year,
                    'label' => $event_year->event_year
                );
            }
        }

        return $options;
    }
}