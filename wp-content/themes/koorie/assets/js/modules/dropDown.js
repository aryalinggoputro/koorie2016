jQuery(document).ready(function($) {
  
    $('select').selectric();

    //reinitialise plugin after form validtion
    $(document).on('click', ".gform_button", function(){
    	setTimeout(
			function()
			{
				$('select').selectric();
			}, 1000);
    });

});
