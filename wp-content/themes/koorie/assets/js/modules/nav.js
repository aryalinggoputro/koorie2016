jQuery(document).ready(function($) {

	'use strict';

	var element = {},
		config = {};

	element.toggle  = $(".site-toggle");
	element.nav		= $(".main-nav");
	element.upArrow	= $(".up-arrow");

	config.state = "is-active";
	config.speed = 0.1; // seconds


	element.toggle.on("click", function(){
		if(this.open){
			this.open = false;
			element.toggle.removeClass(config.state);
			element.nav.removeClass(config.state);
			element.upArrow.show();
		} else {
			this.open = true;
			element.toggle.addClass(config.state);
			element.nav.addClass(config.state);
			element.upArrow.hide();
		}
	});

	$(".expand > span").on("click", function(){
		$(this).parent().toggleClass('is-open');
	});



	config.winWidth = $(document).width();
	config.winHeight = $(window).height() - 55;

	if (config.winWidth < 991) {
		$('.main-nav').css("height", config.winHeight);
	}

	$(window).resize(function() {

			config.winWidth = $(document).width();
			config.winHeight = $(window).height() - 55;

	    if (config.winWidth <= 991) {

			$('.main-nav').css("height", config.winHeight);

		} else if (config.winWidth > 991) {

			$('.main-nav').css("height", "48px");

		}
	});

});
