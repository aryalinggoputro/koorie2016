jQuery(document).ready(function($) {

    'use strict';

    var lastPoint = 0;
    var $asideHeight = $('aside').height();
    var $footerPosition = $('#bottom').offset().top;
    var containerHeight = $('aside');

    if( $('aside').length > 0){
      var containerHeight = $('aside').parents('div.row')[0].clientHeight;
    }

    $(window).on('resize', function(){
      if( $('aside').length > 0){
        containerHeight = $('aside').parents('div.row')[0].clientHeight;
      }
    });

    $('aside').parent().height(containerHeight);

    // var windowSize = $(window).width();
    //
    // $(window).on('resize', function(){
    //   windowSize = $(window).width();
    // })

    $(window).scroll(function(event) {


      //sTop = scroll top, sb = scroll bottom
       var sTop = $(this).scrollTop();

       if (sTop > lastPoint) {

            if( sTop > 105 ){
              $('.sticky').addClass('is-active');
              $('header').addClass('sticky-active');
            }

            // if (sTop > 105 && windowSize < 992){
            //
            // }

           if( sTop > 184 ) {

             $('aside').addClass('is-sticky');

            }

            if( sTop + 184 + $asideHeight > $footerPosition + 100) {

              $('aside').removeClass('is-sticky');

              $('aside').addClass('stuck');

             }

       } else {

          if( sTop < 105 ){
            $('.sticky').removeClass('is-active');
            $('header').removeClass('sticky-active');
          }

          if( sTop < 208 ){ $('aside').removeClass('is-sticky'); }


          if( sTop + 184 + $asideHeight < $footerPosition + 100 && sTop > 184) {

            $('aside').addClass('is-sticky');

            $('aside').removeClass('stuck');


           }
       }

       lastPoint = sTop;

    });



});
