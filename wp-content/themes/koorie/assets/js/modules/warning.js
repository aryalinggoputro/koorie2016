jQuery(document).ready(function($) {

	'use strict';

 	if (typeof $.cookie('Koorie-Warning') === 'undefined'){
 		//no cookie found
 		$('.warning').show();
	} else {
		//cookie found
	 	$('.warning').hide();
	}

    $('#warning-btn').on('click', function(e){

    	e.preventDefault();

    	var box = $('#readWarning').is(":checked");

    	if(box) {
    		//checked
    		$('.warning').hide();
    		$.cookie('Koorie-Warning', 'accepted', { path: '/' });
    	} else {
            console.log('hi Declan');
    		$('.validation-message').addClass('is-active');
    	}
    });

});
