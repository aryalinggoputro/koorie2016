jQuery(document).ready(function($) {

	'use strict';

	// adds placeholders for older versions of IE
	$('input, textarea').placeholder();

});
