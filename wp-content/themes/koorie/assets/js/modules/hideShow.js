jQuery(document).ready(function($) {

    'use strict';

    // Object for configuration storage
    var element = {};

	element.container = $('.expandable-container');
	element.trigger   = element.container.find('.showhide-trigger');

	element.trigger.on('click', function() {
	  	element.container.toggleClass('is-active');
	});

});
