jQuery(document).ready(function($) {

    'use strict';

	$('input').iCheck({
		checkboxClass: 'icheckbox_polaris',
		radioClass: 'iradio_polaris',
		increaseArea: '-10%' // optional
	});

});
