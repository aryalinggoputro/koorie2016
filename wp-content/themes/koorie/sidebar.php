<?php
/**
 * Template for sidebars used throughout the
 * site by various templates.
 *
 * @package koorie
 */


/**
 * Subnav sidebar
 *
 * This logic will determine if the current page is on the
 * top tier and has children or is a sibling of children to
 * a top tier page on the main navigation menu. Renders the
 * subnavigation placing the parent as the above link and
 * the children in menu order if present.
 */
if( ThemeTemplateUtilities::check_sidebar_usage( 'subnav' ) ) {

    ThemeTemplateUtilities::get_partial('subnav');
}