/**
 * Created by ash on 15/04/15.
 */

// Force lightbox on all images within content pages

$(document).ready(function($){
    $('#main .col-md-12 img').each(function(){

        var imgLink = $(this).attr('src'),
            imgAlt = $(this).attr('alt');

        $(this).wrap("<a class='gallery-item' href=" + imgLink + " data-lightbox='gallery' data-title='" + imgAlt + "'></a>");

    });
});